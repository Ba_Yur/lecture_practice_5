import 'package:flutter/material.dart';
import 'package:lecture_practice_5/providers/counter_provider.dart';
import 'package:provider/provider.dart';

import 'firstScreen.dart';

class SecondScreen extends StatelessWidget {
  static String routeName = '/second-page';

  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Builder(
        builder: (contx) => Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: () {counterProvider.decreaseCounter();
                Scaffold.of(contx).hideCurrentSnackBar();
                Scaffold.of(contx).showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.blue,
                    content: Text('Counter deceased. Counter value is: ${counterProvider.counter}'),
                    duration: Duration(milliseconds: 2000),
                    action: SnackBarAction(
                      label: 'Ok',
                      onPressed: () {},
                    ),
                  ),
                );},
                child: Icon(Icons.remove),
              ),
              ElevatedButton(
                onPressed: () {
                  counterProvider.increaseCounter();
                  Scaffold.of(contx).hideCurrentSnackBar();
                  Scaffold.of(contx).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.redAccent,
                      content: Text('Counter deceased. Counter value is: ${counterProvider.counter}'),
                      duration: Duration(milliseconds: 2000),
                      action: SnackBarAction(
                        label: 'Ok',
                        onPressed: () {},
                      ),
                    ),
                  );
                },
                child: Icon(Icons.add),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
