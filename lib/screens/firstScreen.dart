import 'package:flutter/material.dart';
import 'package:lecture_practice_5/providers/counter_provider.dart';
import 'package:lecture_practice_5/screens/second_screen.dart';
import 'package:provider/provider.dart';

class FirstScreen extends StatelessWidget {

  static String routeName = '/first-page';

  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context);

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(icon: Icon(Icons.arrow_forward), onPressed: (){
            Navigator.of(context).pushNamed(SecondScreen.routeName);
          })
        ],
        title: Text('First Page'),
      ),
      body: Center(
        child: Text(counterProvider.counter.toString(),
        style: TextStyle(
          fontSize: 50,
        ),),
      ),
    );
  }
}
