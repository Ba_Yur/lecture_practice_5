
import 'package:flutter/cupertino.dart';

class CounterProvider with ChangeNotifier {

  int _counter = 0;

  int get counter {
    return _counter;
  }


  void increaseCounter() {
    _counter++;
    notifyListeners();
  }

  void decreaseCounter() {
    _counter--;
    notifyListeners();
  }


}