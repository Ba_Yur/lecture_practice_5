import 'package:flutter/material.dart';
import 'package:lecture_practice_5/providers/counter_provider.dart';
import 'package:lecture_practice_5/screens/firstScreen.dart';
import 'package:provider/provider.dart';

import 'screens/second_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CounterProvider(),
      child: MaterialApp(
        initialRoute: FirstScreen.routeName,
        // home: MyHomePage(),
        routes: {
          FirstScreen.routeName: (ctx) => FirstScreen(),
          SecondScreen.routeName: (ctx) => SecondScreen(),
        },
      ),
    );
  }
}
